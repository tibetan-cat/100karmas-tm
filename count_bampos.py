from pathlib import Path


def count_bampos(filename):
    def find_markup_end(line):
        a = line.rfind(']')
        b = line.rfind('}')
        if a > b:
            return a
        else:
            return b

    filename = Path(filename)
    lines = list(filename.open())

    cleaned = [['bampo', 'start', 'end', 'lines', 'folio sides', 'folios']]
    prev = ''
    b_count = -1
    prev_b_lines = 0
    num = 0
    for line in lines:
        if not line.endswith(']\n'):
            num += 1
        if 'བམ་པོ' in line or 'པོ་བཅུ་བཞི་པ། ' in line:
            b_count += 1
            start = find_markup_end(line)
            page, text = line[:start+1], line[start+1:]
            page = page[page.rfind('[')+1:page.rfind(']')]

            if b_count > 0:
                line_count = num + 1 - prev_b_lines
                side_count = line_count // 7
                page_count = side_count / 2
                cleaned.append([b_count, prev, page, line_count, side_count, page_count])
                prev_b_lines = num + 1
            prev = page

    total_lines = 0
    total_sides = 0
    total_folios = 0
    for _, _, _, lines, sides, folios in cleaned[1:]:
        total_lines += lines
        total_sides += sides
        total_folios += folios
    cleaned.append(['', '', '', '', '', ''])
    cleaned.append(['Total:', '', '', total_lines, total_sides, total_folios])

    Path('bampos.csv').write_text('\n'.join([','.join([str(l) for l in line]) for line in cleaned]))


if __name__ == '__main__':
    count_bampos('../raw_texts/T340.txt')
