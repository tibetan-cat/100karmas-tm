from pathlib import Path


def chunk(filename, outdir):
    MARK = '། །།'
    filename = Path(filename)

    content = filename.read_text()
    chunks = [chunk + MARK for chunk in content.split(MARK)]
    for num, chunk in enumerate(chunks):
        Path(Path(outdir) / f'{filename.stem}-{num + 1}-{filename.suffix}').write_text(chunk)


if __name__ == '__main__':
    chunk('../raw_texts/T340.txt', 'stories')
    chunk('../raw_texts/T340_raw.txt', 'stories_raw')
